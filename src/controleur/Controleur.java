package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JButton;

import modele.Agenda;
import modele.Date;
import modele.Event;
import vue.AjoutEvent;
import vue.PanelFrise;
import vue.PanelDiaporama;

/**
 * La classe <b>Controleur<b> et la classe qui gère les actions effectuées sur le bouton d'ajout d'évènements et sur la fenêtre "pop-up" qui apparait lorsque l'on créer un nouvel évènement 
 *
 */

public class Controleur implements ActionListener, modele.ConstantesCalendrier {
	
	Agenda agenda;
	AjoutEvent AjoutEvent;
	PanelDiaporama PanelDiaporama;
	
	/**
	 * Le <i>constructeur<i> <b>Controleur()<b>, de la classe <b>Controleur<b>, surveille les actions sur le bouton "ajout" du <i>formulaire<i> et surveille les action sur les éléments de l'affichage du calendrier.
	 * 
	 * @param parAgenda Une copie de l'<i>agenda<i> contenant tous les évènements créés.
	 * @param parPanelFormulaire Le <i>formulaire<i> qui permet de créer un nouvel évènement.
	 * @param parPanelCalendrier Le panneau affichant tous les jours de toutes les semaines du mois séléctionné.
	 * @param parPanelAffichage Le contenu de l'onglet de l'<i>agenda<i>.
	 */
	public Controleur (Agenda parAgenda, AjoutEvent parAjoutEvent, PanelDiaporama parPanelDiaporama) {
		
		agenda = parAgenda;
		AjoutEvent = parAjoutEvent;
		PanelDiaporama = parPanelDiaporama;
		AjoutEvent.enregistreEcouteur(this);  // le controleur s'enregistre à l'écoute des actions sur le formulaire
	}//fin constructeur Controleur

	/**
	 * La méthode <b>actionPerformed<b> permet de gérer les actions effectuées sur le bouton "ajouter" du <i>formulaire<i> et des boutons du <i>calendrier<i>.
	 * @param parEvt L'évènement
	 */
	@Override
	public void actionPerformed(ActionEvent parEvt) {
		AjoutEvent.getChampRecevantFocus().requestFocus();
		if (parEvt.getActionCommand().equals(INTITULE_BOUTON_AJOUT)) {		
				 Event evt = AjoutEvent.getEvent();
				 agenda.ajout (evt);
				 //System.out.println(agenda);
				 JOptionPane.showMessageDialog((JButton)parEvt.getSource(), agenda.toString());
				 AjoutEvent.reset();
		 		 PanelDiaporama.setDate(AjoutEvent.getDate());
		 		 System.out.println(AjoutEvent.getDate());
			}//fin if
		 	
	} //fin actionPerformed	
		
}//fin classe Controleur

 