package vue;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * La classe PanelDiaporama est la classe qui repr�sente le haut de l'application </br>
 * En effet, comme l'on a explique dans la classe PanelFrise, nous avons coupe en deux parties la fenetre, </br>
 * la partie du haut contiendra : sur les cotes il y aura les fl�ches, en haut il y aura le titre, et au centre  </br>
 * il y aura le diaporama qui sera compose d'une image et de ses caracteristiques.
 */
@SuppressWarnings({ "serial", "unused" })
public class PanelDiaporama extends JPanel {
	
	PanelOuest panelOuest;
	PanelNord panelNord;
	PanelCentre panelCentre;
	PanelEst panelEst;
	
	/**
	 * Constructeur
	 */
	public PanelDiaporama() {
		setLayout(new BorderLayout());
		
		
		panelNord = new PanelNord();
		panelOuest = new PanelOuest();
		panelCentre = new PanelCentre();
		panelEst = new PanelEst();
		
		this.add(panelNord, BorderLayout.NORTH);
		this.add(panelCentre, BorderLayout.CENTER);
		this.add(panelOuest, BorderLayout.WEST);
		this.add(panelEst, BorderLayout.EAST);
		

		
	}//fin contructeur PanelDiaporama



}//fin classe PanelDiaporama
