package vue;

import javax.swing.JFrame ;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import modele.ConstantesCouleursFontes;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;

/**
 * Ceci est la FenetreMere, c'est la classe principale de notre projet. </br>
 * Elle compose principalement notre application (dans le contentPane), ainsi qu'une barre de menu qui </br>
 * propose differentes options : </br>
 * - Ajouter un évènement </br>
 * - Quitter l'application </br>
 * - En savoir plus sur l'application </br>
 */
@SuppressWarnings({ "serial", "unused" })
public class FenetreMere extends JFrame implements ConstantesCouleursFontes, ActionListener { 
	
	JMenuBar barre;
	JMenu menu;
	JMenuItem ajout;
	JMenuItem quitter;
	JMenuItem aide;
	JOptionPane optionPane1;
		
	/**
	 * La méthode <b>FenetreMere()<b> est le constructeur de la classe <b>FenetreMere<b>.
	 * le constructeur est public.
	 * 
	 * @param parTitre De type <b>String<b> il contient le nom de la page.
	 * 
	 */
	public FenetreMere (String parTitre) {
		super (parTitre); 
	    	    
		barre = new JMenuBar();
		menu = new JMenu("MENU (Cliquez ici)");
		ajout = new JMenuItem("Ajout");
		quitter = new JMenuItem("Quitter");
		aide = new JMenuItem("Aide");
		
		barre.add(menu);
		menu.add(ajout);
		menu.add(quitter);
		menu.add(aide);
		
		
		ajout.setActionCommand("Ajout");
		ajout.addActionListener(this);
		quitter.setActionCommand("Quitter");
		quitter.addActionListener(this);		
		aide.setActionCommand("Aide");
		aide.addActionListener(this);
		setJMenuBar(barre);

		
		PanelFrise contentPane  = new PanelFrise(); //appel constructeur JPanel
		setContentPane(contentPane);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(1000,800);
		setVisible(true);
		
		setLocation(200,50);
		setBackground (MASTIC);
		
	}//fin FenetreMere ()
	 	
	/**
	 * <i>Main<i> classique de la frise
	 * 
	 * @param args peut contenir des eventuels arguments spécifiés au lancement du programme.
	 * 
	 */
	public static void main (String  [] args) {
	 	new FenetreMere ("Frise chronologique du COVID-19");
		
	}//fin main()

	/**
	 * La méthode <b>Insets()<b> permet de definir les bordures de la page entre le contenu et 10 pixels après les bord
	 */
	public Insets getInsets() {
		return new Insets(32,10,10,10);
	}//fin Insets

	
	/**
	 * La methode actionPerformed, ici, sert à faire reagir les boutons du menu. </br>
	 * Cliquer sur Ajout permet d'ajouter un évènement. </br>
	 * Cliquer sur Quitter permet de fermer l'application. </br>
	 * Cliquer sur Aide permet d'en savoir plus sur l'application.
	 */
	@SuppressWarnings("static-access")
	@Override
	public void actionPerformed(ActionEvent evt) {		
		if (evt.getActionCommand().equals("Ajout")) {
			optionPane1 = new JOptionPane();
			optionPane1.showMessageDialog(null, "Ajout d'un évènement", "Information",  JOptionPane.INFORMATION_MESSAGE);
			
		}//fin if
		if (evt.getActionCommand().equals("Quitter")) {
			 System.exit(0);
		}//fin if
		if (evt.getActionCommand().equals("Aide")) {
			optionPane1 = new JOptionPane();
			optionPane1.showMessageDialog(null, "Le COVID-19 est un maladie cause par le virus SARS-CoV-2, \n" + 
					"elle fait partie de la famille des Coronavirus, \n" + "et semble avoir apparu au plus tard en novembre 2019. \n" + "" +
					"Cette aplication permet nottement de pouvoir observer l'evolution dans le monde \n" + "de la maladie et de ses effets sur le monde. \n" + 
					"Vous pouvez ajouter des evenements qui vous sembles important \n" + "ou simplement consulter ceux deja entres. \n",
					"AIDE",  JOptionPane.INFORMATION_MESSAGE);
			
		}//fin if
	}//fin actionPerformed
	
	
}//fin FenetreMere
