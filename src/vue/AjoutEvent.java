package vue;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import javax.swing.*;

import controleur.Controleur;
import modele.ConstantesCalendrier;
import modele.ConstantesCouleursFontes;
import modele.Date;
import modele.Agenda;
import modele.Event; 

@SuppressWarnings({ "serial", "unused" })
/**
 * La classe AjoutEvent permet creer, placer et gerer le formulaire de saisie qui permet d'ajouter un nouvel element à la frise
 */
public class AjoutEvent extends JPanel implements ConstantesCalendrier, ConstantesCouleursFontes, ActionListener {
	JTextField titretxt;
	JTextField lieutxt;
	JComboBox<String> jourCombo;
	JComboBox<String> moisCombo;
	JComboBox<String> anneeCombo;
	JTextArea descriptiontxt;
	JButton actualiser;
	JButton ajouter;
	Agenda agenda;
	JLabel labelTitre;
	Date date;
	
	/**
	 * Constructeur de la classe AjoutEvent qui place tout ses elements
	 * @param parAgenda objet de type <b>Agenda</b>
	 * @param parDate object de type <b>Date</b>
	 */
	public AjoutEvent(Agenda parAgenda, Date parDate) {
		GridBagLayout grid = new GridBagLayout();
		setLayout(grid);
		
		agenda=parAgenda;
		
		date = parDate;
		
		JLabel titre = new JLabel("Titre");
		titretxt = new JTextField();
		titre.setDisplayedMnemonic('T');
		titre.setLabelFor(titretxt);
		titretxt.setFocusable(true);
		titretxt.requestFocus();
		
		JLabel lieu = new JLabel("Lieu");
		lieutxt = new JTextField();
		lieu.setDisplayedMnemonic('L');
		lieu.setLabelFor(lieutxt);
		lieutxt.setFocusable(true);

		JLabel dateLabel = new JLabel("Date");
		dateLabel.setDisplayedMnemonic('D');
		
		int dernierJour = Date.dernierJourDuMois(date.getMois(), date.getAnnee());
		String[] jour = new String [dernierJour];
		String [] mois = new String [12];
		String [] annee = new String [10];
		
		int i;
		for(i=0; i<dernierJour; i++) {
			jour[i]=Integer.toString(i+1);	
			
			if(i<12)
				mois[i]=Integer.toString(i+1);
			
			if(i<10)
				annee[i]=Integer.toString(date.getAnnee()+i);
		}//fin for
		
		jourCombo = new JComboBox<String> (jour);
		moisCombo = new JComboBox<String> (mois);
		anneeCombo = new JComboBox<String> (annee);	
		
		jourCombo.setSelectedIndex(date.getJour()-1);
		moisCombo.setSelectedIndex(date.getMois()-1);
		anneeCombo.setSelectedIndex(0);
		
		JLabel description = new JLabel("Description");
		descriptiontxt = new JTextArea(7,15);
		description.setDisplayedMnemonic('e');
		description.setLabelFor(descriptiontxt);
		descriptiontxt.setFocusable(true);
		descriptiontxt.setLineWrap(true);

		labelTitre = new JLabel(date.toString());
		
		ajouter = new JButton("Ajouter");
		ajouter.addActionListener(this);
		ajouter.setFocusable(false);
		ajouter.setMnemonic('A');
		
		actualiser = new JButton("Actualiser");
		actualiser.addActionListener(this);
		actualiser.setFocusable(false);
		actualiser.setMnemonic('c');
		
		GridBagConstraints contrainte = new GridBagConstraints();
		
		//contrainte.gridheight=GridBagConstraints.RELATIVE;
		
		setLabelTitre(0,0,contrainte);
		add(labelTitre, contrainte);
		
		setBouton(4,0,contrainte);
		add(ajouter, contrainte);
		
		setLabel(0,1,contrainte);
		add(titre, contrainte);
		
		setText(1,1,contrainte);
		add(titretxt, contrainte);
		
		setLabel(0,2,contrainte);
		add(lieu, contrainte);

		setText(1,2,contrainte);
		add(lieutxt, contrainte);
		
		setLabel(0,3,contrainte);
		add(dateLabel, contrainte);	
		
		setCombo(1,3,contrainte);
		add(jourCombo, contrainte);
		setCombo(2,3,contrainte);
		add(moisCombo, contrainte);
		setCombo(3,3,contrainte);
		add(anneeCombo, contrainte);
		
		setBouton(4,3,contrainte);
		add(actualiser, contrainte);
		
		setLabel(0,4,contrainte);
		add(description, contrainte);
		
		setText(1,4,contrainte);
		add(descriptiontxt, contrainte);
		
	}//fin AjoutEvent

	/**
	 * La methode <b>reset<b> permet de vider les champs du <i>formulaire<i> lorsque les informations contenues dans ceux-ci sont enregistrés dans l'<i>agenda<i>.
	 */
	public void reset(){
		
		titretxt.setText(new String()); 
		lieutxt.setText(new String());
		descriptiontxt.setText(new String());
			
		// l'heure en cours est sélectionnée
		GregorianCalendar calendar = new GregorianCalendar();
		jourCombo.setSelectedIndex(0);
		moisCombo.setSelectedIndex(0);
		anneeCombo.setSelectedIndex(0);
		
		// le focus est donné au premier champ de saisie
		titretxt.requestFocus();
	}//fin reset
	
	/**
	 * setCombo permet de restaurer la position des contraintes des champs de saisie
	 * @param x entier
	 * @param y entier
	 * @param contrainte objet de type GridBagConstraints
	 */
	private void setLabel(int x, int y, GridBagConstraints contrainte) {
		contrainte.anchor=GridBagConstraints.FIRST_LINE_START;
		
		contrainte.insets=new Insets(10,10,10,10);
		
		contrainte.gridx=x;
		contrainte.gridy=y;
		
		contrainte.gridheight=1;
		contrainte.gridwidth=1;
		
		contrainte.fill=GridBagConstraints.NONE;		
	}//fin setLabel
	
	/**
	 * setCombo permet de restaurer la position des contraintes des champs de saisie
	 * @param x entier
	 * @param y entier
	 * @param contrainte objet de type GridBagConstraints
	 */
	private void setLabelTitre(int x, int y, GridBagConstraints contrainte) {
		contrainte.anchor=GridBagConstraints.CENTER;

		contrainte.insets=new Insets(10,10,10,10);
		
		contrainte.gridx=x;
		contrainte.gridy=y;
		
		contrainte.gridheight=1;
		contrainte.gridwidth=3;
		
		contrainte.fill=GridBagConstraints.NONE;		
	}//fin setLabelTitre
	
	/**
	 * setCombo permet de restaurer la position des contraintes des boutons
	 * @param x entier
	 * @param y entier
	 * @param contrainte objet de type GridBagConstraints
	 */
	private void setBouton(int x, int y, GridBagConstraints contrainte) {
		contrainte.anchor=GridBagConstraints.FIRST_LINE_END;
		contrainte.insets=new Insets(10,10,10,10);
		
		contrainte.gridx=x;
		contrainte.gridy=y;
				
		contrainte.gridheight=1;
		contrainte.gridwidth=1;
		contrainte.fill=GridBagConstraints.BOTH;
	}//fin setBouton
	
	/**
	 * setCombo permet de restaurer la position des contraintes de la boite de texte de description
	 * @param x entier
	 * @param y entier
	 * @param contrainte objet de type GridBagConstraints
	 */
	private void setText(int x, int y, GridBagConstraints contrainte) {
		contrainte.anchor=GridBagConstraints.CENTER;
		contrainte.insets=new Insets(10,10,10,50);
		
		contrainte.gridx=x;
		contrainte.gridy=y;
				
		contrainte.gridheight=1;
		contrainte.gridwidth=4;
		contrainte.fill=GridBagConstraints.BOTH;
	}//fin setText
	
	/**
	 * setCombo permet de restaurer la position des contraintes des comboBox
	 * @param x entier
	 * @param y entier
	 * @param contrainte objet de type GridBagConstraints
	 */
	private void setCombo(int x, int y, GridBagConstraints contrainte) {
		contrainte.anchor=GridBagConstraints.CENTER;
		contrainte.insets=new Insets(10,10,10,10);
		
		contrainte.gridx=x;
		contrainte.gridy=y;
		
		contrainte.gridheight=1;
		contrainte.gridwidth=1;
				
		contrainte.anchor=GridBagConstraints.CENTER;
	}//fin setCombo
	
	/**
	 * actionPermormed de la classe AjoutEvent utilisé pour les bouton ajouter et actualiser
	 */
	public void actionPerformed(ActionEvent event) {
		if(event.getSource()==ajouter) {//if1
			
			date.setJour(jourCombo.getSelectedIndex()+1);
			date.setMois(moisCombo.getSelectedIndex()+1);
			date.setAnnee(anneeCombo.getSelectedIndex()+date.getAnnee());
			
			if(date.getJour()<=Date.dernierJourDuMois(date.getMois(), date.getAnnee())) {//if2
				String titre = titretxt.getText();
				String lieu = lieutxt.getText();
				
				agenda.ajout(new Event(date,titre, lieu));
				
				titretxt.setText("");
				lieutxt.setText("");
				descriptiontxt.setText("");
					
				titretxt.requestFocus();
					
				jourCombo.setSelectedIndex(date.getJour()-1);
				moisCombo.setSelectedIndex(date.getMois()-1);
				anneeCombo.setSelectedIndex(0);
				
				remove(labelTitre);
				
				labelTitre.setText(date.toString());
				labelTitre.setForeground(new Color(0,0,0));
				
				GridBagConstraints contrainte = new GridBagConstraints();
				
				setLabelTitre(0,0,contrainte);
				add(labelTitre, contrainte);

				revalidate();
				repaint();
			}//fin if2
			
			else {
				remove(labelTitre);
				labelTitre.setText("Date invalide veuillez actualiser");
				labelTitre.setForeground(new Color(255,0,0));
				
				GridBagConstraints contrainte = new GridBagConstraints();
				
				setLabelTitre(0,0,contrainte);
				add(labelTitre, contrainte);

				revalidate();
				repaint();
				
				jourCombo.setSelectedIndex(date.getJour()-1);
				moisCombo.setSelectedIndex(date.getMois()-1);
				anneeCombo.setSelectedIndex(0);
			}//fin else
		}//fin if1
		
		else if(event.getSource()==actualiser) {
			remove(jourCombo);
			resetJour(moisCombo.getSelectedIndex()+1, anneeCombo.getSelectedIndex()+date.getAnnee());
			
			GridBagConstraints contrainte = new GridBagConstraints();
			setCombo(1,3,contrainte);
			add(jourCombo, contrainte);
			
			revalidate();
			repaint();
		}//fin else if
	}//fin actionPerformed
	
	public void setDate(int jour, int mois, int annee) {
		date.setJour(jour);
		date.setMois(mois);
		date.setAnnee(annee);
	}//fin setDate
	
	/**
	 * resetJour permet de remettre la JComboBox au dernier jour du mois séléctionné
	 * @param parMois entier
	 * @param parAnnee entier
	 */
	private void resetJour(int parMois, int parAnnee) {
		int dernierJour=Date.dernierJourDuMois(parMois, parAnnee);
		
		String[] jour = new String [dernierJour];
		
		int i;
		for(i=0; i<dernierJour; i++) {
			jour[i]=Integer.toString(i+1);	
		}//fin for
		
		jourCombo = new JComboBox<String> (jour);
		
		if(date.getJour()<dernierJour)
			jourCombo.setSelectedIndex(date.getJour()-1);
		else
			jourCombo.setSelectedIndex(dernierJour-1);

	}//fin else
}//fin classe Ajout Event
