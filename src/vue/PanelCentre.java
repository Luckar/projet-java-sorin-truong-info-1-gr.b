package vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;


/**
 * La classe PanelCentre est celle qui repr�sente l'image de l'evenement en question, ainsi que ses 4 caracteristiques, </br>
 * qui sont le titre de l'evnement, sa date, son lieu, et sa description.
 */
@SuppressWarnings({ "serial", "unused" })
public class PanelCentre extends JPanel  {
	
	JLabel labelImage;
    JLabel labelTitre;
	JLabel labelDate;
	JLabel labelLieu;
	JLabel labelDescription;
	JTextArea zoneTexte;
	final String INTITULE_REPERTOIRE_IMAGES = "imagesEvents";
	
	/**
	 * Constructeur
	 */
	public PanelCentre() {		
		ImageIcon image = new ImageIcon(new ImageIcon(INTITULE_REPERTOIRE_IMAGES + File.separator + "covid-19.png")
				.getImage().getScaledInstance(194, 250, Image.SCALE_DEFAULT));
		
		labelImage = new JLabel (image); 		
		//labelImage = new JLabel ("*IMAGE*", JLabel.LEFT);
		//labelImage.setFont(new Font("Arial",Font.BOLD,80));
		labelImage.setSize(50, 50);
		this.add(labelImage, "covid-19.png" );
		
		
		
		labelTitre = new JLabel ("      *TITRE*", JLabel.CENTER);
		labelTitre.setFont(new Font("Arial",Font.BOLD,50));
		//this.add(labelTitre);
		
		labelDate = new JLabel ("      *DATE*", JLabel.CENTER);
		labelDate.setFont(new Font("Arial",Font.BOLD,40));
		//this.add(labelDate);
		
		labelLieu = new JLabel ("      *LIEU*", JLabel.CENTER);
		labelLieu.setFont(new Font("Arial",Font.BOLD,40));
		//this.add(labelLieu);
		
		labelDescription = new JLabel ("      *DESCRIPTION*", JLabel.CENTER);
		labelDescription.setFont(new Font("Arial",Font.BOLD,60));
		//this.add(labelDescription);
		 
				
		//Ce qui est dans la JTextArea sont les labels		
		zoneTexte = new JTextArea(labelTitre.getText() + "\n" + labelDate.getText() + "\n" + labelLieu.getText() + "\n" + labelDescription.getText()); 
		zoneTexte.setBackground(null);
		zoneTexte.setFont(new Font("Arial",Font.BOLD,40));
		this.add(zoneTexte);
		
	}//fin constructeur PanelCentre


	/**
	 * La methode <b>Insets()<b> permet de definir les bordures de la page entre le contenu et 10 pixels apr�s les bord
	 */
	public Insets getInsets() {
		return new Insets(10,10,10,10);
	}//fin Insets

	

}//fin classe PanelCentre
