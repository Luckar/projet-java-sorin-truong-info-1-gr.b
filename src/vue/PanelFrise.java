package vue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modele.Agenda;
import modele.ConstantesCouleursFontes;

/**

 * Ce PanelFrise est celui qui descend directement de la FenetreMere, il divise la fenetre en deux parties : </br>
 * dans la partie du haut il y aura le diaporama, les fleches, le titre, et dans la partie du bas il y aura le tableau et ses evenements.
 */
@SuppressWarnings({ "serial", "unused" })
public class PanelFrise extends JPanel implements ActionListener, ConstantesCouleursFontes {
	PanelDiaporama panelDiaporama;
	PanelTableau panelTableau;
	
	
	
	/**
	 * Constructeur </br>
	 * Le <i>constructeur<i> de la classe <b>PanelAgenda<b>, <b>PanelAgenda()<b>, cr�er le contenu de l'onglet de l'agenda, </br>
     * dont le <i>formulaire<i>, le panneau du <i>calendrier<i> et le tableau affichant les <i>evenements<i> de la semaine s�lectionne. </br>
	 */
	public PanelFrise(){
		
		Agenda agenda = new Agenda();
		setBackground(VANILLE);
		
		
		
		panelDiaporama = new PanelDiaporama();
		panelTableau = new PanelTableau();

		
		//GridLayout de tailles suivantes : 2 de hauteur et 1 de largeur
		//Ceci est fait pour diviser la fen�tre en deux parties, la partie du haut sera le diaporama, 
		//et la partie du bas sera le tableau
		setLayout(new GridLayout(2,1));		
		this.add(panelDiaporama);
		this.add(panelTableau);
		
		
		
	}//fin du constructeur de actionPerformed



	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}//fin actionPerformed

}//fin classe actionPerformed
