package vue;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * Le PanelNord sera simplement composee du titre de notre application.
 */
@SuppressWarnings({ "serial", "unused" })
public class PanelNord extends JPanel  {
	
	JLabel labelTitre;

	/**
	 * Constructeur
	 */
	public PanelNord() {
		
		setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
		
		// Le titre 
		labelTitre = new JLabel ("Frise chronologique du COVID-19", JLabel.CENTER);
		labelTitre.setFont(new Font("Arial",Font.BOLD,40));
		
		
		this.add(labelTitre);

		
	}//fin constructeur PanelNord


}//fin classe PanelNord
