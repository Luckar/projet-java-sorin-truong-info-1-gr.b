package vue;

import java.util.Collection;


import javax.swing.table.DefaultTableModel;

import modele.Agenda;
import modele.Date;
import modele.Event;

/**
 * La classe ModeleTable represente le modele de la table que l'on se sert dans PanelTableau. </br>
 * Par exemple, ici, le modele de notre tableau sera un tableau de 5 lignes et 26 colonnes.
 */
@SuppressWarnings({ "serial", "unused" })
public class ModeleTable extends DefaultTableModel  {

	/**
	 * Constructeur
	 */
	public ModeleTable() {
		
		setColumnCount(26);
		setRowCount(5);
	
	}//fin constructeur de ModeleTable
	
	
	/**
	 * La methode isCellEditable sert � savoir si la case du tableau, selon la ligne et la colonne indiqu�e, est vide ou pas.
	 */
	public boolean isCellEditable(int indiceLigne, int indiceColonne) {
		return false;
	}//fin isCellEditable
	
	
	/**
	 * La m�thode getColumnClass sert a recuperer la classe de l'evenement. </br>
	 * Elle prend en parametre un entier.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int parNum) {
		return Event.class;
	}//fin getColumnClass
	

}//fin classe ModeleTable
