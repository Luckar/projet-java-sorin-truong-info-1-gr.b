package vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * La classe PanelEst est simplement composee d'une fleche qui servira a passer a l'evenement suivant.
 */
@SuppressWarnings({ "serial", "unused" })
public class PanelEst extends JPanel implements ActionListener {
	
	
	JButton boutonSuivant;

	/**
	 * Constructeur
	 */
	public PanelEst() {
		boutonSuivant = new JButton (">");
		boutonSuivant.setFont(new Font("Arial",Font.BOLD,40));
		boutonSuivant.addActionListener(this);
		this.add(boutonSuivant);

	}//fin constructeur PanelEst

	
	/**
	 * La methode <b>Insets()<b> permet de definir les bordures de la page entre le contenu et 10 pixels apr�s les bord 
	 */
	public Insets getInsets() {
		return new Insets(80,10,10,10);
	}//fin Insets

	/**
	 * La methode actionPerformed, ici, sert a faire reagir le boutonSuivant (la fleche) et cela permettra de passer a l'evenement suivant.
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}//fin actionPerformed
}
//fin classe PanelEst
