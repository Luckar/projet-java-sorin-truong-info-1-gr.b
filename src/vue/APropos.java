package vue;

import java.awt.*; 
import javax.swing.*;

@SuppressWarnings("serial")
/**
 * la classe APropos est appelée pour afficher une descrtiption de la frise et de ses otpions (théoriquement dans une fenetre "pop-up")
 *
 */
public class APropos extends JPanel {
	

	JLabel labelTitre;
	JLabel labelDescription;
	
		/**
		 * Le <i>constructeur<i> de la classe <b>APropos<b>, <b>APropos()<b>, affiche une description du logiciel.
		 */
	public APropos(){
		
		// Le gestionnaire de repartition
		GridBagConstraints contraintes = new GridBagConstraints();
	  	contraintes.insets = new Insets (6,6,6,6); 
		contraintes.anchor = GridBagConstraints.WEST;
		
		
		// Les element graphiques		 
	 	JLabel labelTitre = new JLabel ("Frise chronologique du COVID-19", JLabel.LEFT);
	 	labelTitre.setFont(new Font("Arial",Font.BOLD,30));
		this.add(labelTitre);
		JLabel labelDescription1 = new JLabel ("Le COVID-19 est un maladie causÃ©e par le virus SARS-CoV-2, ", JLabel.LEFT);
		labelDescription1.setFont(new Font("Arial",Font.BOLD,20));
		this.add(labelDescription1);
		JLabel labelDescription2 = new JLabel ("elle fait partie de la famille des Coronavirus, ", JLabel.LEFT);
		labelDescription2.setFont(new Font("Arial",Font.BOLD,20));
		this.add(labelDescription2);
		JLabel labelDescription3 = new JLabel ("et semble avoir apparu au plus tard en novembre 2019.", JLabel.LEFT);
		labelDescription3.setFont(new Font("Arial",Font.BOLD,20));
		this.add(labelDescription3);
		JLabel labelDescription4 = new JLabel ("Cette aplication permet nottement de pouvoir observer l'Ã©volution dans le monde", JLabel.LEFT);
		labelDescription4.setFont(new Font("Arial",Font.BOLD,20));
		this.add(labelDescription4);
		JLabel labelDescription5 = new JLabel ("de la maladie et de ses effets sur le monde.", JLabel.LEFT);
		labelDescription5.setFont(new Font("Arial",Font.BOLD,20));
		this.add(labelDescription5);
		JLabel labelDescription6 = new JLabel ("Vous pouvez ajouter des Ã©vÃ¨nements qui vous sembles important", JLabel.SOUTH);
		labelDescription6.setFont(new Font("Arial",Font.BOLD,20));
		this.add(labelDescription6);
		JLabel labelDescription7 = new JLabel (" ou simplement consulter ceux dÃ©jÃ  entrÃ©s.", JLabel.SOUTH);
		labelDescription7.setFont(new Font("Arial",Font.BOLD,20));
		this.add(labelDescription7);
			
			
		    
		}//fin du constructeur de PanelFormulaire


}//fin classe AP
