package vue;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * La classe PanelOuest est simplement composee d'une fleche qui servira a passer a l'evenement precedent.
 */
@SuppressWarnings("serial")
public class PanelOuest extends JPanel implements ActionListener {
	
	
	JButton boutonPrecedent;

	/**
	 * Constructeur
	 */
	public PanelOuest() {
		boutonPrecedent = new JButton ("<");
		boutonPrecedent.addActionListener(this);
		boutonPrecedent.setFont(new Font("Arial",Font.BOLD,40));
		this.add(boutonPrecedent);

		
	}//fin constructeur PanelOuest

	

	/**
	 * La methode <b>Insets()<b> permet de definir les bordures de la page entre le contenu et 10 pixels apr�s les bords.
	 */
	public Insets getInsets() {
		return new Insets(80,10,10,10);
	}//fin Insets



	/**
	 * La methode actionPerformed, ici, sert a faire reagir le boutonPrecedent (la fleche) et cela permettra de passer a l'evenement precedent.
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}//fin actionPerformed
}//fin classe PanelOuest
