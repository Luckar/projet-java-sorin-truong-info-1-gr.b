package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;


/**
 * Le PanelTableau est le panel qui compose la partie du bas de l'application. </br>
 * En effet, le panelTableau est composee du tableau, ainsi que des evenements qui integreront le tableau.
 */
@SuppressWarnings({ "serial", "unused" })
public class PanelTableau extends JPanel {
	
	JTable table;
	ModeleTable modele;
	
	/**
	 * Constructeur
	 */
	public PanelTableau(){
		
		setLayout(new BorderLayout());
		
		
		modele = new ModeleTable();
		table = new JTable(modele);		
		JScrollPane scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int screenHeight = screenSize.height;
		int screenWidth = screenSize.width;
		
		// On divise l'�cran en 2 puis on divise la moiti� de l'�cran en 5 (nombre de ligne)
		table.setRowHeight((screenHeight/2)/5);

		
		//pour qu'il y ait les scrollBar horizontale et verticales automatiquement
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		table.setRowSelectionAllowed(false);
		table.setColumnSelectionAllowed(false);
		
		
		this.add(scrollPane, BorderLayout.CENTER);
		
		
	}//fin constructeur PanelTableau
	

	/**
	 * La m�thode <b>Insets()<b> permet de definir les bordures de la page entre le contenu et 5 pixels apr�s les bord pour
	 */
	public Insets getInsets() {
		return new Insets(30,30,20,30);
	}//fin Insets

	
}//fin classe PanelTableau
