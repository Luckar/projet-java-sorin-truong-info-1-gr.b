package modele;

public interface ConstantesCalendrier {
	final String [] JOURS_SEMAINE = {"lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"} ;  
	
	final  String [] MOIS = {"janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aot", "septembre", "octobre", "novembre", "dcembre"};
	final String [] JOURS_DU_MOIS = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
	
	final String INTIT_PRECEDENT = "< precedent";
	final String INTIT_SUIVANT = "suivant >";
	final  String [] INTITULES_BOUTONS = {INTIT_PRECEDENT, INTIT_SUIVANT};
	
	final String INTITULE_BOUTON_AJOUT ="+";
	
	public final  String [] HEURES = {"00", "01", "02", "03", "04", "05", "06","07", "08", "09", "10", "11", "12", "13","14", "15", "16", "17", "18", "19", "20","21", "22", "23"};
	public final  String [] MINUTES = {"00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"};
}//fin ConstantesCalendrier
