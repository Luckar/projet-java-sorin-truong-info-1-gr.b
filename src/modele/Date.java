package modele;
import java.util.GregorianCalendar;
import java.util.Calendar;
 
/**
 * La classe Date permet de creer et de gerer des objets representant des dates composees de 3 entiers , le jour, le mois et l'annee
 * 		ainsi que d'une chaine de caracteres coorespondante au nom du jour de la semaine
 */
public class Date implements Comparable <Date> {
	private int jour;
	private int mois;
	private int annee;
	private int jourSemaine ;
	  
	   
	/**
	 * Constructeur de la classe Date ne prend pas d'argument et créer une date à la date du jour.
	 */
	public Date(){
		GregorianCalendar dateAuj = new GregorianCalendar ();
		annee = dateAuj.get (Calendar.YEAR);
		mois = dateAuj.get (Calendar.MONTH)+1; // janvier = 0, fevrier = 1...
		jour = dateAuj.get (Calendar.DAY_OF_MONTH);
		jourSemaine = dateAuj.get (Calendar.DAY_OF_WEEK);
	}//fin constructeur sans parametres
	
	public Date (int parJour, int parMois, int parAnnee){
		jour = parJour;
		mois = parMois;
		annee = parAnnee; 
		GregorianCalendar date = new GregorianCalendar (annee,mois-1,jour);
		jourSemaine = date.get (Calendar.DAY_OF_WEEK);				
	}//fin constructeur Date avec parametres
	
	
	
	/**
	 * retourne 0 si this et parDate sont égales, 
	 * -1 si this précède parDate,
	 *  1 si parDate précède this
	 */
	public int compareTo (Date parDate){
		if (annee < parDate.annee)
			return -1;
		if (annee > parDate.annee)
			return 1;
		// les années sont =
		if (mois < parDate.mois)
			return -1;
		if (mois > parDate.mois)
			return 1;
		// les mois sont =
		if (jour < parDate.jour)
			return -1;
		if (jour > parDate.jour)
			return 1;
		return 0;	
	}//fin compareTo
	
	/**
	 * @return renvoie la date du jour suivant
	 */
	public Date dateDuLendemain(){
		if (jour < dernierJourDuMois(mois,annee))
		return  new Date (jour+1,mois,annee);
		else if (mois < 12)
		return new Date (1,mois+1,annee);
		else return new Date (1,1,annee+1);
	}//fin dateDuLendemain
	
	/**
	 * @return renvoie la date du jour précédent
	 */
	public Date dateDeLaVeille(){
		if (jour > 1)
		return  new Date (jour-1,mois,annee);
		else if (mois > 1)
		return new Date (Date.dernierJourDuMois(mois-1, annee),mois-1,annee);
		else return  new Date (31,12,annee-1);
	}//fin dateDeLaVeille
	
	/**
	 * 
	 * @param parMois entier designant le mois de la date appelante
	 * @param parAnnee entier designant l'annee de la date appelante
	 * @return renvoie un entier coorespondant au noméro du dernier jour du mois de la date appelante
	 */
	public static int dernierJourDuMois (int parMois, int parAnnee){
		switch (parMois){
			case 2 : if (estBissextile (parAnnee))  return 29 ; else return 28 ;  
			case 4 : case 6 : case 9 : case 11 : return 30 ;
			default : return 31 ;
		}//fin switch
	}//fin dernierJourDuMois
	
	private static boolean estBissextile(int parAnnee) {
	return parAnnee % 4 == 0 && (parAnnee % 100 != 0 || parAnnee % 400 == 0);
	}//fin estBissextile
	
	/**
	 * 
	 * @return renvoie pour la date appelante une chaine de caractere contenant le nom du jour, le numéro du jour et le nom du mois.
	 */
	public String toString(){
		String chaine = new String();
		switch(jourSemaine){
			case 1: chaine = "dimanche"; break;
			case 2: chaine = "lundi"; break;
			case 3: chaine = "mardi"; break;
			case 4: chaine = "mercredi"; break;
			case 5: chaine = "jeudi"; break;
			case 6: chaine = "vendredi"; break;
			case 7: chaine = "samedi"; break;
		}//fin switch
		chaine += " " + jour + " ";
		switch(mois){
			case 1: chaine += "janvier"; break;
			case 2: chaine += "février"; break;
			case 3: chaine += "mars"; break;
			case 4: chaine += "avril"; break;
			case 5: chaine += "mai"; break;
			case 6: chaine += "juin"; break;
			case 7: chaine += "juillet"; break;
			case 8: chaine += "août"; break;
			case 9: chaine += "septembre"; break;
			case 10: chaine += "octobre"; break;
			case 11: chaine += "novembre"; break;
			case 12: chaine += "décembre"; break;
		}//fin switch
		return chaine;
	}//fin toString
	
	
	/**
	 * @return renvoie un entier coorespondant à l'annee de la date appelante
	 */
	public int getAnnee(){
		return annee;
	}//fin getAnnee
	
	/**
	 * @return renvoie un entier coorespondant au jour de la date appelante
	 */
	public int getJour(){
		return jour;
	}//fin getJour
	
	/**
	 * @return renvoie un entier coorespondant au mois de la date appelante
	 */
	public int getMois(){
		return mois;
	}//fin getMois
	
	/**
	 * @return renvoie un entier de 0 à 6 respectivement de dimanche à samedi
	 */
	public int getJourSemaine(){
		return jourSemaine;
	}//fin getJourSemaine
	
	/**
	 * @return renvoie un booléen, à 1 si la date appelante est la même que la date en parametre
	 */
	public boolean isToday(){
		return new Date().compareTo(this) == 0;
	}//fin isToday
	
	/**
	 * setJour permet de changer la valeur du jour
	 * @param parJour entier
	 */
	public void setJour(int parJour){
		jour = parJour;
	}//fin setJour
	
	/**
	 * setMois permet de changer la valeur du mois
	 * @param parMois entier
	 */
	public void setMois(int parMois){
		mois = parMois;
	}//fin setMois
	
	/**
	 * setAnnee permet de changer la valeur de l'annee
	 * @param parAnnee entier
	 */
	public void setAnnee(int parAnnee){
		annee = parAnnee; 
	}//fin setAnnee


}//fin classe Date