package modele;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class LectureEcriture {
	/**
	 * Méthode de lecture d’un fichier
	 * @param parFichier : le fichier lu
	 * @return l’objet lu
	 * */
	public static Object lecture (File parFichier){
		ObjectInputStream flux;
		Object objetLu = null;
		
		// Ouverture du fichier en mode lecture
		try {
			flux = new ObjectInputStream(new FileInputStream (parFichier));
			objetLu = (Object)flux.readObject();
			flux.close();
		}//fin try
		
		catch (ClassNotFoundException parException){
			System.err.println (parException.toString());
			System.exit (1);
		}//fin catch
		
		catch (IOException parException){
			System.err.println ("Erreur lecture du fichier " + parException.toString());
			System.exit (1);
		}//fin atch
		
		return objetLu ;
	}//fin lecture
	
	/**
	 * Méthode d’écriture dans un fichier
	 * @param parFichier : le fichier dans lequel on écrit
	 * @param parObjet : l’objet écrit dans le fichier
	 */
	
	public static void ecriture (File parFichier, Object parObjet){
		ObjectOutputStream flux = null;
		
		// Ouverture du fichier en mode écriture
		try {
			flux = new ObjectOutputStream (new FileOutputStream (parFichier));
			flux.writeObject (parObjet);
			flux.flush();
			flux.close();
		}//fin try
		
		catch (IOException parException){
			System.err.println ("Probleme a l’ecriture\n" + parException.toString());
			System.exit (1);
		}//fin catch
	}//fin ecriture
}//fin classe LectureEcriture
