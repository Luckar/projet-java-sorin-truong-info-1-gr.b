package modele;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Set;

/**
 * La classe <b>Agenda<b> permet de creer et gerer des objet qui representes des agenda d'evenements.
 *
 */
public class Agenda{
	
	private ArrayList <Event> listeEvts ;
	private TreeSet <Event> arbreEvts;
	private HashMap <Integer, ArrayList<Event>> hashMapEvts;
	@SuppressWarnings("unused")
	private TreeMap <Date, ArrayList<Event>> treeMapEvts;
	
	/**
	 * La methode <b>Agenda<b> permet de creer un nouvel Agenda contenant des dates dans des listes.
	 * 
	 */
	public Agenda(){
		
		listeEvts = new ArrayList<>();
		arbreEvts = new TreeSet<>();
		hashMapEvts = new  HashMap<>();
		treeMapEvts = new TreeMap<>();
	}//fin constructeur d'Agenda
	
	
	/**
	 * La methode <b>ajout<b> permet d'ajouter un evenement donne a l'agenda.
	 * 
	 * @param parEvt Une objet evenement
	 */
	public void ajout (Event parEvt){
		
		// Ajout dans la ArrayList
		listeEvts.add (parEvt);
		// Ajout dans le TreeSet
		arbreEvts.add (parEvt);
		
		// Ajout dans la HashMap :
		// clef : le numero de semaine
		// valeur : les evenements qui ont lieu cette semaine
		
		Date date = parEvt.getDate();
		GregorianCalendar calendar = new GregorianCalendar(date.getAnnee(), date.getMois()-1, date.getJour());
		int numeroDeSemaine = calendar.get(Calendar.WEEK_OF_YEAR);
		
		
		if (hashMapEvts.containsKey(numeroDeSemaine)){ 		
			hashMapEvts.get(numeroDeSemaine).add (parEvt);	
		}//fin if
		else {
			ArrayList <Event> liste = new ArrayList <Event> ();
			liste.add (parEvt);
			getHashMapEvts().put (numeroDeSemaine, liste);
		}//fin else
	}//fin ajout
	
	
	// parcours de la hashmap avec un iterateur sur l'ensemble des clefs
	
	/**
	 * La methode <b>toString<b> permet de recuperer la liste des tous les evenements en une seule chaine de caractere. 
	 * 
	 * @return Un objet de type <b>String<b>
	 */
	@Override
	public String toString(){
		String chaine =  "\n" + listeEvts + "\n" + arbreEvts + "\n\n";
		
		Set <Integer> clefs = getHashMapEvts().keySet ();
		Iterator <Integer> iterateur =clefs.iterator(); 
		while (iterateur.hasNext()){
			Integer clef = iterateur.next();
			ArrayList <Event> liste = getHashMapEvts().get(clef);
			chaine += clef + " : " + liste + "\n";
		}//fin while
		return chaine;
	}//fin toString
	
	
	
	/**
	 * La methode <b>compteNbEvt<b> permet de recuperer le nombre d'evenements de l'agenda qui ont lieu a une date donnee.
	 * 
	 * @param parDate Une date donnee
	 * @return Un entier representant un nombre de dates
	 */
	public int compteNbEvt (Date parDate){
		int nbEvt = 0;
		Iterator <Event> iterateur = arbreEvts.iterator();
		while (iterateur.hasNext()) {
			Event evt = iterateur.next();
			if (evt.getDate().compareTo (parDate) == 0)
				nbEvt++;
		}//fin while	
		return nbEvt;
	}//fin compteNbEvt
	
	/**
	 * La methode <b>compteNbEvt<b> permet de recuperer le nombre d'evenements de l'agenda dont le titre contient une chaine de caracateres donne.
	 * 
	 * @param parString Une chaine de caractere representant un nom  
	 * @return Un entier representant un nombre de dates
	 */
	public int compteNbEvt (String parString){
		int nbEvt = 0;
		for (Event evt : listeEvts){
			if (evt.getTitre().contains (parString)){
				nbEvt++;
			}//fin if
		}//fin for
		return nbEvt;
	}//fin compteNbEvt
	
	
	/**
	 * La methode <b>trifusion<b> permet de trier une liste d'evenement.
	 * 
	 * @param list
	 * @param indiceDebut
	 * @param longueur
	 * @return Une liste d'evenements
	 */
	public ArrayList <Event> triFusion (ArrayList <Event>  list, int indiceDebut, int longueur){
		if (longueur == 1){
			ArrayList <Event> arrayList =new ArrayList <Event> ();
			arrayList.add(list.get(indiceDebut));
			return arrayList;
		}//fin if
		else
			return fusion (triFusion(list,indiceDebut ,longueur/2), triFusion (list, indiceDebut + longueur/2, longueur-longueur/2));
	}//fin triFusion
	
	/**
	 * La methode <b>fusion<b> permet de fusionner deux listes d'evenements.
	 * 
	 * @param liste1 Une liste d'evenments
	 * @param liste2 Une liste d'evnments
	 * @return Une liste d'evenements contenant le contenu des deux listes en parametres
	 */
	public ArrayList <Event> fusion (ArrayList <Event> liste1, ArrayList <Event> liste2){
		if (liste1.isEmpty()){
			return liste2;
		}//fin if
		if (liste2.isEmpty()){
			return liste1;
		}//fin if
		ArrayList <Event> arrayList =new ArrayList <Event> (); 
		Event premierDeListe1 = liste1.get(0);
		Event premierDeListe2 = liste2.get(0);
		if (premierDeListe1.compareTo(premierDeListe2) <= 0) {
			liste1.remove(0);			
			arrayList = fusion(liste1,liste2);
			arrayList.add(0,premierDeListe1);
		}//fin if
		else{
			liste2.remove(0);
			arrayList = fusion(liste1,liste2);
			arrayList.add(0,premierDeListe2);
		}//fin else
		return arrayList;		  
	}//fin fusion
	
	
	/**
	 * La methode <b>getHashMapEvts<b> permet de recuperer la liste contenant les evenements crees.
	 * 
	 * @return Une liste de type <b>hashMapEvts<b> contenant des evenements
	 */
	public HashMap <Integer, ArrayList <Event>> getHashMapEvts(){
		return hashMapEvts;
	}//fin getHashMapEvts
	
	/**
	 * La methode <b>setHashMapEvts<b> permet de modifier la liste de type <b>hashMapEvts<b> des evenements.
	 * 
	 * @param hashMapEvts contenant la liste des evenements crees
	 */
	public void setHashMapEvts(HashMap <Integer, ArrayList <Event>> hashMapEvts) {
		this.hashMapEvts = hashMapEvts;
	}//fin setHashMapEvts
	
	/**
	 * La methode <b>getEvents<b> permet de recuperer une liste ordonnee contenant les evenements crees.
	 * 
	 * @return Liste de type <b>Treeset<b> d'evenements
	 */
	public Collection <Event> getEvents(){
		return arbreEvts;
	}//fin getEvents
	
	/**
	 * La methode <b>getEventsSemaine<b> permet de recuperer le numero des semaines de l'annee du numero de semaine donne en parametre. 
	 * 
	 * @param numSemaine
	 * @return Une liste d'entiers representants des numeros de semaines
	 */
	public Collection <Event> getEventsSemaine (int numSemaine){
		return hashMapEvts.get(numSemaine);
	}//fin getEventsSemaine
	
	/**
	 * La methode <b>getEventsSemaine<b> permet de recuperer le numero des semaines de l'annee de la date donnee en parametre. 
	 * 
	 * @param date
	 * @return Une liste d'entiers representants des numeros de semaines
	 */
	public Collection <Event> getEventsSemaine (Date date){
		GregorianCalendar calendar = new GregorianCalendar(date.getAnnee(), date.getMois()-1, date.getJour());
		int numSemaine = calendar.get(Calendar.WEEK_OF_YEAR);
		return hashMapEvts.get(numSemaine);
	}//fin getEventsSemaine

}//fin classe Agenda