package modele;

/**
 * Classe qui permet de creer et gerer des objets qui representent des evenement avec un nom d'evenement, un lieu et une date (de type <b>Date</b>)
 *
 */
public class Event{
	Date chDate;
	String chTitre;
	String chLieu;
	static int chNbEvtInstancies = 0;

	/**
	 * Constructeur de la classe Event 
	 * @param parDate objet de type <b>Date</b>
	 * @param parTitre chaine de caracteres
	 * @param parLieu chaine de caracters
	 */
	public Event(Date parDate, String parTitre, String parLieu){
		chDate=parDate;
		chTitre=parTitre;
		chLieu=parLieu;
		chNbEvtInstancies++;
	}//fin constructeur Event

	/**
	 * permet de comparer de deux evenment pour determiner l'anteriorite de l'un par rapport à l'autre
	 * @param evenement2
	 * @return renvoie un booléen 
	 * 		- renvoi 1 si l'event appelant est anterieur au parametre,
	 * 		- renvoi -1 si l'event appelant est posterieur au parametre,
	 * 		- renvoi 0 si les event sont à la même date.
	 */
	public int compareTo(Event evenement2){
		int resultatComp=chDate.compareTo(evenement2.chDate);
		if(resultatComp!=0)
			return resultatComp;

		resultatComp=chTitre.compareTo(evenement2.chTitre);
		if(resultatComp!=0)
			return resultatComp;

		resultatComp=chLieu.compareTo(evenement2.chLieu);
		if(resultatComp!=0)
			return resultatComp;
		return 0;	
	}//fin compareTo

	/**
	 * @return renvoie une chaine de caracteres qui contient le nom de l'evenement, sa date, le lieu de l'evenement ainsi que le nombre total d'evenement instancies  
	 */
	public String toString(){
		return chTitre+" : "+chDate+", "+chLieu+"\n Nb d'event prevus : "+chNbEvtInstancies;
	}//fin toString

	/**
	 * @return renvoie un objet de type <b>Date</b> qui represente la date de l'evenement
	 */
	public Date getDate(){
		return chDate;
	}//fin getDate

	/**
	 * @return renvoie une chaine de caractere contenant le nom de l'evenement
	 */
	public String getTitre(){
		return chTitre;
	}//fin getTitre

	/**
	 * @return renvoie une chaine de caractere contenant le nom du lieu de l'evenement
	 */
	public String getLieu(){
		return chLieu;
	}//fin getLieu

	/**
	 * setDate permet de modifier la Date de l'evenement
	 * @param parDate de type <b>Date</b>
	 */
	public void setDate(Date parDate){
		chDate=parDate;
	}//fin setDate

	/**
	 * setTitre permet de modifier la valeur du nom de l'evenement
	 * @param parTitre de type String
	 */
	public void setTitre(String parTitre){
		chTitre=parTitre;
	}//fin setTitre

	/**
	 * setLieu permet de modifier la valeur du nom du lieu de l'evenement
	 * @param parLieu de type String
	 */
	public void setLieu(String parLieu){
		chLieu=parLieu;
	}//fin setLieu
}//fin classe Event